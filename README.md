# Test Job Instructions #

Follow the instructions and complete the following tasks. 

Throughout the steps where there is code to add or is changed, you should commit each of the steps.

Deadline for completing the steps is 07.06.16 at 14:00.

### Version Control (Git) ###

* Use your favorite version control software (SourceTree etc.)
* Create your own Repo on BitBucket or GitHub

### Setting up Laravel ###

* Setup laravel in the cloned repo
* Setup your local environment file, database and server 

### Add Bootstrap ###

* Add bootstrap to the body of the page, for simple styling

(Tip! You can use cdn for simpler addition of it)

### Create a User System ###

* Create a simple login and signup authentication system

(Tip! Laravel provides a boilerplate for a user system, see the Laravel documentation for more information)

### Create a CRUD ###

* Create a migration for "clubs", with the following fields "id", "name" and "description"
* Create ability to create a "club", where the user can write the "name" and "description" of this "club"
* Create ability to see all the "clubs" created
* Create ability to see a single "club"
* Create ability to edit an already created "club"
* Create ability to delete an already created "club"

### Extra ###

* Make only registered users able to create, edit and delete "clubs"

### Complete ###

* Once all the steps are done, push all steps to your Repo
* Send email to Kasper (Kasper@lycaste.co) with the url to your Repo.

If you have any questions regarding the steps don't hesitate to contact Kasper (Kasper@lycaste.co).